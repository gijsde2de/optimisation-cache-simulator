#include "template.h"

// instantiate the cache
Cache cache;

// helper functions; forward all requests to the cache
uint LOADINT( uint address ) { return cache.Read32bit( address ); }
void STOREINT( uint address, uint value ) { cache.Write32bit( address, value ); }

// ============================================================================
// CACHE SIMULATOR IMPLEMENTATION
// ============================================================================

// cache constructor
Cache::Cache()
{
	RAM = (uint*)MALLOC64( 1024 * 1024 * 4 );
	memset(RAM, 0, 1024 * 1024 * 4);

	memset(cache, 0, sizeof(CacheLine) * NUMSLOTS);
}

// cache destructor
Cache::~Cache()
{
	printf("hits: %i\n", hits);
	printf("misses: %i\n", misses);
	system("pause");
	printf( "%f\n", dummy ); // prevent dead code elimination
}

#ifdef LRU
static void ASSIGN_QUEUE(unsigned int slot, unsigned int offset, CacheLine* cache)
{
	for (unsigned int i = 0; i < SETSIZE; i++)
		if (cache[slot + i].valid)
			cache[slot + i].lru++;

	cache[slot + offset].lru = 0;
}
#endif

static uint EVICT(CacheLine* cache, unsigned int slot)
{
#ifdef LRU
	unsigned int max = 0;
	unsigned int j;
	for (unsigned int i = 0; i < SETSIZE; i++)
		if (cache[slot + i].lru > max)
		{
			max = cache[slot + i].lru;
			j = slot + i;
		}

	return j;
#elif defined MRU
	return cache[slot].mru;
#elif defined RANDOM
	return slot + RAND(SETSIZE);
#endif
}

// reading 32-bit values
uint Cache::Read32bit( uint address )
{
#ifdef SET_ASSOCIATIVE
	uint i = ((address >> 6) & SLOTMASK) * SETSIZE;
	uint slot;
	for (int j = 0; j < SETSIZE; ++j) {
		slot = i + j;
		if (cache[slot].valid) {
			if ((cache[slot].tag & ADDRESSMASK) == (address & ADDRESSMASK)) {
				hits++;
#ifdef LRU
				ASSIGN_QUEUE(i, j, cache);
#elif defined MRU
				cache[slot].mru = i;
#endif
				return cache[slot].data[(address & 63) >> 2];
			}
		}
	}

	// cache read miss: read data from RAM
	misses++;
	__declspec(align(64)) CacheLine line;
	LoadLineFromMem(address, line);
	uint value = line.data[(address & 63) >> 2];

	for (int j = 0; j < SETSIZE; ++j) {
		slot = i + j;
		if (!cache[slot].valid) {
			memcpy(cache[slot].data, line.data, SLOTSIZE);
			cache[slot].tag = address;
			cache[slot].valid = true;
			cache[slot].dirty = false;
#ifdef LRU
			ASSIGN_QUEUE(i, j, cache);
#elif defined MRU
			cache[slot].mru = i;
#endif
			return value;
		}
	}

	i = EVICT(cache, i);

	if (cache[i].dirty) {
		WriteLineToMem(cache[i].tag, cache[i]);
	}

	memcpy(cache[i].data, line.data, SLOTSIZE);
	cache[i].tag = address;
	cache[i].valid = true;
	cache[i].dirty = false;
#ifdef LRU
	ASSIGN_QUEUE(i, i % SETSIZE, cache);
#elif defined MRU
	cache[slot].mru = i;
#endif
	

	return value;

#endif

#ifdef DIRECT_MAPPING
	uint i = ((address >> 6) & SLOTMASK);
	if (cache[i].valid) {
		if ((cache[i].tag & ADDRESSMASK) == (address & ADDRESSMASK)) {
			hits++;
			return cache[i].data[(address & 63) >> 2];
		}
	}

	misses++;
	__declspec(align(64)) CacheLine line;
	LoadLineFromMem(address, line);
	uint value = line.data[(address & 63) >> 2];

	if (!cache[i].valid) {
		memcpy(cache[i].data, line.data, SLOTSIZE);
		cache[i].tag = address;
		cache[i].valid = true;
		cache[i].dirty = false;
		return value;
	}

	if (cache[i].dirty) {
		WriteLineToMem(cache[i].tag, cache[i]);
	}

	memcpy(cache[i].data, line.data, SLOTSIZE);
	cache[i].tag = address;
	cache[i].valid = true;
	cache[i].dirty = false;

	return value;

#endif

#ifdef FULLY_ASSOCIATIVE
	for (int i = 0; i < NUMSLOTS; ++i) {
		if (cache[i].valid) {
			if ((cache[i].tag & ADDRESSMASK) == (address & ADDRESSMASK)) {
				hits++;
				ASSIGN_QUEUE(i, 0, cache);
				return cache[i].data[(address & 63) >> 2];
			}
		}
	}

	// cache read miss: read data from RAM
	misses++;
	__declspec(align(64)) CacheLine line;
	LoadLineFromMem( address, line );
	uint value = line.data[(address & 63) >> 2];

	for (int i = 0; i < NUMSLOTS; ++i) {
		if (!cache[i].valid) {
			memcpy(cache[i].data, line.data, SLOTSIZE);
			cache[i].tag = address;
			cache[i].valid = true;
			cache[i].dirty = false;
			ASSIGN_QUEUE(i, 0, cache);
			return value;
		}
	}

	int i = EVICT(cache, 0);

	if (cache[i].dirty) {
		WriteLineToMem(cache[i].tag, cache[i]);
	}

	memcpy(cache[i].data, line.data, SLOTSIZE);
	cache[i].tag = address;
	cache[i].valid = true;
	cache[i].dirty = false;
	ASSIGN_QUEUE(i, 0, cache);

	return value;
#endif

#ifdef NO_CACHING
	__declspec(align(64)) CacheLine line;
	LoadLineFromMem(address, line);
	return line.data[(address & 63) >> 2];
#endif

}

// writing 32-bit values
void Cache::Write32bit( uint address, uint value )
{
#ifdef SET_ASSOCIATIVE
	uint i = ((address >> 6) & SLOTMASK) * SETSIZE;
	uint slot;

	for (int j = 0; j < SETSIZE; ++j) {
		slot = i + j;
		if (cache[slot].valid) {
			if ((cache[slot].tag & ADDRESSMASK) == (address & ADDRESSMASK)) {
				hits++;
				cache[slot].data[(address & 63) >> 2] = value;
				cache[slot].dirty = true;
#ifdef LRU
				ASSIGN_QUEUE(i, j, cache);
#elif defined MRU
				cache[slot].mru = i;
#endif
				return;
			}
		}
	}

	misses++;
	__declspec(align(64)) CacheLine line;
	LoadLineFromMem(address, line);
	line.data[(address & 63) >> 2] = value;

	for (int j = 0; j < SETSIZE; ++j) {
		slot = i + j;
		if (!cache[slot].valid) {
			memcpy(cache[slot].data, line.data, SLOTSIZE);
			cache[slot].tag = address;
			cache[slot].valid = true;
			cache[slot].dirty = true;
#ifdef LRU
			ASSIGN_QUEUE(i, j, cache);
#elif defined MRU
			cache[slot].mru = i;
#endif
			return;
		}
	}

	i = EVICT(cache, i);

	if (cache[i].dirty) {
		WriteLineToMem(cache[i].tag, cache[i]);
	}

	memcpy(cache[i].data, line.data, SLOTSIZE);
	cache[i].tag = address;
	cache[i].valid = true;
	cache[i].dirty = true;
#ifdef LRU
	ASSIGN_QUEUE(i, i % SETSIZE, cache);
#elif defined MRU
	cache[slot].mru = i;
#endif
	return;

#endif
#ifdef DIRECT_MAPPING
	uint i = ((address >> 6) & SLOTMASK);
	if (cache[i].valid) {
		if ((cache[i].tag & ADDRESSMASK) == (address & ADDRESSMASK)) {
			hits++;
			cache[i].data[(address & 63) >> 2] = value;
			cache[i].dirty = true;
			return;
		}
	}

	misses++;
	__declspec(align(64)) CacheLine line;
	LoadLineFromMem(address, line);
	line.data[(address & 63) >> 2] = value;

	if (!cache[i].valid) {
		memcpy(cache[i].data, line.data, SLOTSIZE);
		cache[i].tag = address;
		cache[i].valid = true;
		cache[i].dirty = true;
		return;
	}
	
	if (cache[i].dirty) {
		WriteLineToMem(cache[i].tag, cache[i]);
	}

	memcpy(cache[i].data, line.data, SLOTSIZE);
	cache[i].tag = address;
	cache[i].valid = true;
	cache[i].dirty = true;
	return;

#endif

#ifdef FULLY_ASSOCIATIVE
	for (int i = 0; i < NUMSLOTS; ++i) {
		if (cache[i].valid) {
			if ((cache[i].tag & ADDRESSMASK) == (address & ADDRESSMASK)) {
				hits++;
				cache[i].data[(address & 63) >> 2] = value;
				cache[i].dirty = true;
				ASSIGN_QUEUE(i, 0, cache);
				return;
			}
		}
	}

	misses++;
	__declspec(align(64)) CacheLine line;
	LoadLineFromMem(address, line);
	line.data[(address & 63) >> 2] = value;

	for (int i = 0; i < NUMSLOTS; ++i) {
		if (!cache[i].valid) {
			memcpy(cache[i].data, line.data, SLOTSIZE);
			cache[i].tag = address;
			cache[i].valid = true;
			cache[i].dirty = true;
			ASSIGN_QUEUE(i, 0, cache);
			return;
		}
	}

	// evict random cacheline
	int i = EVICT(cache, 0);
	if (cache[i].dirty) {
		WriteLineToMem(cache[i].tag, cache[i]);
	}

	memcpy(cache[i].data, line.data, SLOTSIZE);
	cache[i].tag = address;
	cache[i].valid = true;
	cache[i].dirty = true;
	ASSIGN_QUEUE(i, 0, cache);
	return;
#endif

#ifdef NO_CACHING
	__declspec(align(64)) CacheLine line;
	LoadLineFromMem(address, line);
	line.data[(address & 63) >> 2] = value;
	WriteLineToMem(address, line);
#endif

}

// ============================================================================
// CACHE SIMULATOR LOW LEVEL MEMORY ACCESS WITH LATENCY SIMULATION
// ============================================================================

// load a cache line from memory; simulate RAM latency
void Cache::LoadLineFromMem( uint address, CacheLine& line )
{
	uint lineAddress = address & 0xFFFFFFC0; // set last six bit to 0
	memcpy( line.data, &RAM[lineAddress / 4], 64 ); // fetch 64 bytes into line
	DELAY;
}

// write a cache line to memory; simulate RAM latency
void Cache::WriteLineToMem( uint address, CacheLine& line )
{
	uint lineAddress = address & 0xFFFFFFC0; // set last six bit to 0
	memcpy( &RAM[lineAddress / 4], line.data, 64 ); // write line
	DELAY;
}