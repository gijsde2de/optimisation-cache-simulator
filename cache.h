#pragma once
#include <random>

uint LOADINT( uint address );
void STOREINT( uint address, uint value );

#define DELAY dummy = min( 10, dummy + sinf( (float)(address / 1789) ) ); // artificial delay
#define L1CACHE			8192
#define SLOTSIZE		64
#define NUMSLOTS		L1CACHE / SLOTSIZE
#define ADDRESSMASK		0xFFFFFFC0
#define SETSIZE			4
#define SLOTMASK		(NUMSLOTS / SETSIZE - 1)

//#define FULLY_ASSOCIATIVE
//#define DIRECT_MAPPING
//#define NO_CACHING
#define SET_ASSOCIATIVE

//#define LRU
//#define MRU
#define RANDOM
#define RAND(S) ((unsigned int)(std::rand() / (double(RAND_MAX) + 1.0) * S))

struct CacheLine
{
	uint tag;
#ifdef LRU
	uint lru;
#endif
#ifdef MRU
	uint mru;
#endif
	uint data[SLOTSIZE / 4]; // 64 bytes
	bool valid, dirty;
};

class Cache
{
public:
	Cache();
	~Cache();
	void ArtificialDelay();
	uint Read32bit( uint address );
	void Write32bit( uint address, uint value );
	float GetDummyValue() const { return dummy; }
	CacheLine cache[NUMSLOTS];
	int hits, misses = 0;
private:
	void LoadLineFromMem( uint address, CacheLine& line );
	void WriteLineToMem( uint address, CacheLine& line );
	uint* RAM;
	float dummy;
};